FROM python:3.10
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY requirements.txt /code/
RUN pip install pip==23.1.1
RUN pip install -r requirements.txt
RUN git config --global --add safe.directory /code
