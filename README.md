# Portal

Portal est un portail web.

Il est développé initialement par et pour l'association des Éclaireuses et Éclaireurs de France.
Mais il est générique et peut être configuré pour convenir à d'autres associations.

N'hésite pas à contribuer en créant une issue ou en proposant une merge request sur https://gitlab.com/eedf/portal/.


# Dévelopment

## Installation

Sur une distribution GNU/Linux Ubuntu, dans un shell, exécute les commandes suivantes :

```bash
$ sudo apt install git docker.io docker-compose
$ git clone https://gitlab.com/eedf/www/portal.git
$ cd portal
$ sudo docker-compose -f docker-compose-dev.yml build
$ sudo docker-compose -f docker-compose-dev.yml run --rm django ./manage.py migrate
$ sudo docker-compose -f docker-compose-dev.yml run --rm django ./manage.py createsuperuser
```

Choisis le numéro d'adhérent du premier utilisateur, par exemple « 1 ».
Il aura tous les droits d'administration.

## Démarrage

Place toi dans le répertoire `portal` puis exécute :

```bash
$ sudo docker-compose -f docker-compose-dev.yml up
```

Ouvre ton navigateur à l'adresse http://localhost:8000

## Mise à jour

Place toi dans le répertoire `portal` puis exécute :

```bash
$ git pull
$ sudo docker-compose -f docker-compose-dev.yml build
$ sudo docker-compose -f docker-compose-dev.yml run --rm django ./manage.py migrate
```
