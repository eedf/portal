#!/usr/bin/env python3

# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Portal.
#
# Portal is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

import os
import sys


if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "portal.settings.prod")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
