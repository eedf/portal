# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Portal.
#
# Portal is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

import pkg_resources  # part of setuptools
from functools import cache
from django.conf import settings
from websites.models import Site


@cache
def get_version():
    return pkg_resources.require("portal")[0].version


def conf(request):
    return {
        'conf': {
            'test_site': settings.TEST_SITE,
            'test_site_banner': settings.TEST_SITE_BANNER,
            'version': get_version(),
            'jeito_host': settings.JEITO_HOST,
            'host': settings.HOST,
        },
        'social_networks': Site.objects.filter(social=True).order_by('order', 'id'),
    }
