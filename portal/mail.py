from django.conf import settings
from django.core.mail.backends.smtp import EmailBackend


class TestEmailBackend(EmailBackend):
    def send_messages(self, email_messages):
        for message in email_messages:
            message.subject = "[Portal-test] " + message.subject
            message.to = settings.TEST_EMAILS
            message.cc = []
            message.bcc = []
        super().send_messages(email_messages)
