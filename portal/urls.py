# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Portal.
#
# Portal is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView


def crash(request):
    division_by_zero = 1 / 0  # noqa


urlpatterns = [
    path('admin/', admin.site.urls),
    path('oidc/', include('oauth2_authcodeflow.urls')),
    path('legal/', TemplateView.as_view(template_name='legal.html'), name='legal'),
    path('crash/', crash),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        path('__debug__', include(debug_toolbar.urls)),
    ]
