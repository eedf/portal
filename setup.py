# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Portal.
#
# Portal is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

# coding: utf8

import os
from setuptools import find_packages, setup

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as f:
    README = f.read()

setup(
    name='portal',
    setup_requires=['setuptools-git-versioning'],
    version_config={
        'dev_template': '{tag}+dev',
        'dirty_template': '{tag}+dev',
    },
    packages=find_packages(),
    include_package_data=True,
    license='AGPL-3.0-or-later',
    description='Web application portal',
    long_description=README,
    url='https://gitlab.com/eedf/portal',
    author='Éclaireuses Éclaireurs de France, Gaël Utard',
    author_email='gael.utard@eedf.fr',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.10',
    ],
    install_requires=[
        'crispy-bootstrap3',
        'Django==4.2',
        'django-oauth2-authcodeflow',
        'django-autocomplete-light',
        'django-crispy-forms',
        'django-filter',
        'django-formtools==2.3',  # stick to 2.3 because of https://github.com/jazzband/django-formtools/issues/220
        'django-localflavor',
        'django-maintenance-mode',
        'djangorestframework',
        'drf-spectacular',
        'gunicorn',
        'num2words',
        'openpyxl',
        'pillow',
        'psycopg2',
        'sentry-sdk',
        # test
        'flake8',
        'factory-boy',
        'freezegun',
        'coverage',
        # dev
        'django-debug-toolbar',
        'django-extensions',
        'ipython',
        'pip-tools',
    ],
)
