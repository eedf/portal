from django.apps import AppConfig


class WebsitesConfig(AppConfig):
    name = 'websites'
    verbose_name = "Web sites"
    default_auto_field = 'django.db.models.BigAutoField'
