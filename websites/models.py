from django.db import models


class Site(models.Model):
    title = models.CharField("Titre", max_length=100)
    description = models.TextField("Description", blank=True)
    url = models.URLField("URL", blank=True, unique=True)
    image = models.ImageField("Image", blank=True, upload_to='portal/')
    order = models.IntegerField("Numéro d'ordre", null=True, blank=True)
    social = models.BooleanField("Réseau social", default=False)
