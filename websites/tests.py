from django.test import TestCase
from websites.models import Site


class PortalTests(TestCase):
    def test_portal(self):
        Site.objects.create(url='https://toto.com', title="Toto", description="Blah")
        response = self.client.get('/')
        self.assertContains(response, "Bienvenue")
        self.assertContains(response, "Toto")
        self.assertContains(response, "Blah")
