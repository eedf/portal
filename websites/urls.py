# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Portal.
#
# Portal is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from django.urls import path, include
from . import views

app_name = 'websites'


urlpatterns = [
    path(r'', include('portal.urls')),
    path(r'', views.PortalView.as_view(), name='home'),
    path(r'update/', views.PortalUpdateView.as_view(), name='portal_update'),
    path(r'site/create/', views.SiteCreateView.as_view(), name='site_create'),
    path(r'site/update/<int:pk>/', views.SiteUpdateView.as_view(), name='site_update'),
    path(r'site/delete/<int:pk>/', views.SiteDeleteView.as_view(), name='site_delete'),
]
