from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import PermissionRequiredMixin
from .models import Site


class PortalView(ListView):
    queryset = Site.objects.filter(social=False).exclude(url='').order_by('order', 'id')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['myself'], _ = Site.objects.get_or_create(
            url='',
            defaults={
                'title': "Portail",
                'description': "Bienvenue."
            }
        )
        return context


class PortalUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'websites.change_site'
    fields = ('title', 'description')
    success_url = reverse_lazy('home')

    def get_object(self):
        return Site.objects.get(url='')


class SiteCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'websites.add_site'
    model = Site
    fields = ('title', 'description', 'image', 'url', 'order')
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        response = super().form_valid(form)
        if "social-network" in self.request.GET:
            self.object.social = True
            self.object.save()
        return response


class SiteUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'websites.change_site'
    model = Site
    fields = ('title', 'description', 'image', 'url', 'order')
    success_url = reverse_lazy('home')


class SiteDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'websites.delete_site'
    model = Site
    success_url = reverse_lazy('home')
